﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;
public class Agent : MonoBehaviour
{
    public Transform target; //target reference
    public float limit = 1f; //far-close tunable threshold

    private float dist; //distance between agent and target
    private FiniteStateMachine brain;

    //Agent initialization
    private void Start()
    {
        brain = new FiniteStateMachine();
        brain.Init();
        brain.SetupTransitions();                        
    }

    //Agent logic
    private void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, target.position, 0.05f); //Agent movement
        dist = Vector2.Distance(target.position, transform.position); //distance update

        if (dist > limit) // activating FAR state
        {
            brain.TriggerTransition("fCtF");
            brain.TriggerTransition("fTtF");
        }
        else
        {
            if (dist == 0) // activating TOUCH state
            {
                brain.TriggerTransition("fCtT");
                brain.TriggerTransition("fFtT");

                TargetReposition();
            }
            else // activating CLOSE state
            {
                brain.TriggerTransition("fFtC");
                brain.TriggerTransition("fTtC");
            }
        }        

        brain.Handle(); // invoke the behaviour
    }

    //Target repositioning
    public void TargetReposition()
    {
        target.position = new Vector3(Random.Range(-2.0f,2.0f), Random.Range(-2.0f, 2.0f), 0);
    }
}
