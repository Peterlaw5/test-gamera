﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

//Concrete state
public class FarState : State
{
    //Concrete behaviour implementation
    public override void Handle()
    {
        Debug.Log("TARGET FAR");
    }
}
