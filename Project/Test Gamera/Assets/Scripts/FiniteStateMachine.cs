﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    public class FiniteStateMachine 
    {

        private State currentState; //the active state at avery frame
        private List<State> states; //the collection of all states
        private List<Transition> transitions; //the collection of all transitions
        

        //FSM states initialization
        public void Init()
        {
            states = new List<State>();

            states.Add(new FarState());
            states.Add(new CloseState());
            states.Add(new TouchState());

            currentState = states[0];
        }

        //FSM transitions initialization
        public void SetupTransitions()
        {
            transitions = new List<Transition>();

            transitions.Add(new Transition("fCtT", states[1], states[2]));
            transitions.Add(new Transition("fFtC", states[0], states[1]));
            transitions.Add(new Transition("fFtT", states[0], states[2]));
            transitions.Add(new Transition("fCtF", states[1], states[0]));
            transitions.Add(new Transition("fTtC", states[2], states[1]));
            transitions.Add(new Transition("fTtF", states[2], states[0]));
        }

        //transition between states
        public void TriggerTransition(string cmd)
        {
            foreach(var tr in transitions)
            {
                if(tr.command == cmd && tr.currentState == this.currentState)
                {
                    currentState = tr.targetState;
                }
            }
        }

        //invoke the active behaviour
        public void Handle()
        {
            currentState.Handle();
        }
    }

    //FSM abstract state declaration
    public abstract class State
    {
        public abstract void Handle();
    }

    //FSM transition declaration
    public class Transition
    {
        public string command;
        public State currentState;
        public State targetState;

        //Transition constructor
        public Transition(string cmd, State currentS, State targetS)
        {
            command = cmd;
            currentState = currentS;
            targetState = targetS;
        }
    }
}
