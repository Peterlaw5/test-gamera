﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

//Concrete state
public class CloseState : State
{
    //Concrete behaviour implementation
    public override void Handle()
    {
        Debug.Log("TARGET CLOSE");
    }
}
